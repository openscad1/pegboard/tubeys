// BOF

include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/pegboard-rail.scad>;

include <tubey.scad>;

chamfer = plate_thickness / 4;

//debug = true;
//slice = true;
//slug = true;

$fn = 360;

inside_diameter = 27; 
h = 5;
height = base_size * h;
how_many_tubes = 2;
how_many_rails = 2;

module slug(color = "cyan") {
    color(color) {
        translate([0, (inside_diameter + plate_thickness)/2, 0]) {
            mcylinder(d1 = 26, d2 = 5, h = 30, align = [0, 0, 1]);
            mcylinder(d = 5, h = 200, align = [0, 0, 1]);
        }
    }
}


main();


// EOF
