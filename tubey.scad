// BOF

// NOSTL

include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/pegboard-rail.scad>;


chamfer = plate_thickness / 4;
wall_thickness = plate_thickness;

//debug = true;
//slice = true;
//slug = true;
if(!is_undef(slice)) {
    echo("slice");
}
if(!is_undef(debug)) {
    echo("debug");
}
if(!is_undef(slug)) {
    echo("slug");
}



module tubey(h = base_size, id = base_size, wall_thickness = wall_thickness, chamfer = chamfer, how_many = 1, color = "orange") {

    if(!is_undef(debug)) {
        echo("debug");
        echo("tubey:              h =", h);
        echo("tubey:             id =", id);
        echo("tubey: wall_thickness =", wall_thickness);
        echo("tubey:        chamfer =", chamfer);
        echo("tubey:       how_many =", how_many);
        echo("tubey:          color =", color);
    }


    color(color) {
        for(x = [1:how_many]) {
            translate([(x - 1) * (id + wall_thickness), 0, 0]) {

                mtube(h = h, od = id + wall_thickness * 2, id = id, align = [0, 0, 1], chamfer = chamfer);
                mcylinder(h = wall_thickness, d = id + wall_thickness * 2, align = [0, 0, 1], chamfer = chamfer);
            }
        }

    }

   
}

module main() {

        intersection() {
            union() {
                // main workpiece
                translate([-((inside_diameter + wall_thickness)/2) * (how_many_tubes - 1), (inside_diameter + wall_thickness)/2, 0]) {
                    tubey(h = height, id = inside_diameter, how_many = how_many_tubes);
                }
                // rails
                color("salmon") {
                    translate([0, -plate_thickness/2, 0]) {
                        rail(height = h, copies = how_many_rails, chamfer = chamfer);
                    }
                }


                // slug
                translate([0, 0, wall_thickness * 2]) {
                    if(!is_undef(slug)) {
                        slug();
                    }
                }


            }

            // slicing block
            if(!is_undef(slice)) {
                mcube([1000, 1000, 1000], align = [1,1,0], color = "pink");
            }
        }
    
}

// EOF
